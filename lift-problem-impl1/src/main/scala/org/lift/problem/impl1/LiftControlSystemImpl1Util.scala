package org.lift.problem.impl1

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.lift.problem.impl1.floor.FloorConfig
import org.lift.problem.impl1.shaft.ShaftSetupConfig
import org.lift.problem.model.floor.Floor
import org.lift.problem.model.lift.{Direction, Route, Lift, LiftState}
import org.lift.problem.model.shaft.{Shaft, ShaftState, ShaftsState}

object LiftControlSystemImpl1Util {

  val liftLoadAtTheInitOfTheSystem = 0 // TODO: should be moved to configuration (LiftControlSystemImpl1Config)

}

protected[impl1] class LiftControlSystemImpl1Util extends LazyLogging {

  import LiftControlSystemImpl1Util._

  /**
   * Creates initial state by converting [[org.lift.problem.impl1.LiftControlSystemImpl1Config]] into
   * [[org.lift.problem.model.shaft.ShaftsState]].
   */
  def createInitialState(config: LiftControlSystemImpl1Config): ShaftsState = {

    val shafts: Seq[ShaftState] = config.map { shaftSetupConfig: ShaftSetupConfig =>

      val shaft: Shaft = {

        val floors: Seq[Floor] = shaftSetupConfig.shaft.floors.map { floorConfig: FloorConfig =>
          new Floor(
            number = floorConfig.number,
            name = floorConfig.name
          )
        }

        new Shaft(
          name = shaftSetupConfig.shaft.name,
          floors = floors
        )
      }

      val liftState: LiftState = {

        val lift: Lift = new Lift(
          name = shaftSetupConfig.lift.name,
          maxCapacity = shaftSetupConfig.engine.maxCapacity,
          currentLoad = liftLoadAtTheInitOfTheSystem
        )

        new LiftState(
          state = Left(shaft.find(_.number == shaftSetupConfig.shaft.liftFloorOnSystemInit).get),
          lift = lift
        )
      }

      new ShaftState(
        shaft = shaft,
        liftState = liftState
      )

    }.toSeq

    new ShaftsState(shafts)
  }

  /**
   * Performs one simulation step.
   *
   * @note 2015-11-23 Artur Stanek: lenses should be used to do deep copying of objects
   */
  def simulationStep(shaftsState: ShaftsState): ShaftsState = {
    logger.debug(s"doing one simulation step in all ${shaftsState.size} shafts")

    shaftsState.copy(
      shafts = shaftsState.shafts.map(simulationStep(_))
    )
  }

  /**
   * Performs one simulation step.
   *
   * @note 2015-11-23 Artur Stanek: lenses should be used to do deep copying of objects
   */
  protected def simulationStep(shaftState: ShaftState): ShaftState = {

    val newState: Either[Floor, Route] = shaftState.liftState.state match {

      // lift is not moving
      case left @ Left(_) => {
        logger.debug(s"lift ${shaftState.liftState} is not moving (so nothing to do)")
        left
      }

      // lift is moving
      case Right(route) => simulationStep(shaftState, route)
    }

    shaftState.copy(
      liftState = shaftState.liftState.copy(
        state = newState
      )
    )
  }

  /**
   * Performs one simulation step.
   */
  protected def simulationStep(shaftState: ShaftState, route: Route): Either[Floor, Route] = shaftState.liftState.direction match {

    // lift is moving
    case Some(direction) => simulationStep(shaftState, route, direction)

    // lift is not moving
    case None => {
      logger.debug(s"lift ${shaftState.liftState} is not moving (so nothing to do)")
      shaftState.liftState.state
    }
  }

  /**
   * Performs one simulation step.
   */
  def simulationStep(
    shaftState: ShaftState,
    route: Route,
    direction: Direction.DirectionType
  ): Either[Floor, Route] = direction match {

    case Direction.Up => shaftState.shaft.followingFloor(route.currentFloor) match {

      case None => {
        logger.debug(s"lift ${shaftState.liftState} reached highest floor (${route.currentFloor}), stopping")
        Left(route.currentFloor)
      }

      case Some(followingFloor) => Right(
        route.copy(
          currentFloor = followingFloor
        )
      )
    }

    case Direction.Down => shaftState.shaft.prependingFloor(route.currentFloor) match {
      case Some(followingFloor) => Right(
        route.copy(
          currentFloor = followingFloor
        )
      )

      case None => {
        logger.debug(s"lift ${shaftState.liftState} reached lowest floor (${route.currentFloor}), stopping")
        Left(route.currentFloor)
      }
    }

  }

  /**
   * Adds destination [[org.lift.problem.model.floor.Floor]] to given [[org.lift.problem.model.lift.Lift]].
   *
   * @note 2015-11-23 Artur Stanek: possible improvement of deep copying by using lenses
   *
   * @param lift which we would like to stop on particular [[org.lift.problem.model.floor.Floor]]
   * @param destinationFloor on which we would like [[org.lift.problem.model.lift.Lift]] to stop
   */
  def addDestinationFloor(
    shaftsState: ShaftsState,
    lift: Lift,
    destinationFloor: Floor
  ): ShaftsState = {

    val previousShaftState: ShaftState = shaftsState.shafts.find(_.liftState.lift == lift).getOrElse(throw new Exception(s"Could not find lift $lift."))

    val newShaftState: ShaftState = addDestinationFloor(previousShaftState, destinationFloor)

    shaftsState.copy(
      shafts = shaftsState.shafts.filterNot(_ == previousShaftState) :+ newShaftState
    )
  }

  /**
   * Adds destination [[org.lift.problem.model.floor.Floor]] to the [[org.lift.problem.model.lift.Lift]].
   *
   * @note 2015-11-23 Artur Stanek: possible improvement of deep copying by using lenses
   *
   * @param destinationFloor on which we would like [[org.lift.problem.model.lift.Lift]] to stop
   */
  protected def addDestinationFloor(
    shaftState: ShaftState,
    destinationFloor: Floor
  ): ShaftState = {

    require(shaftState.shaft.containsFloor(destinationFloor), s"floor on which lift should stop ($destinationFloor) needs to be one of ${shaftState.shaft.floors}")

    shaftState.copy(
      liftState = addDestinationFloor(shaftState.liftState, destinationFloor)
    )
  }

  /**
   * Adds destination [[org.lift.problem.model.floor.Floor]] to the [[org.lift.problem.model.lift.Lift]].
   *
   * @note 2015-11-23 Artur Stanek: possible improvement of deep copying by using lenses
   *
   * @param destinationFloor on which we would like [[org.lift.problem.model.lift.Lift]] to stop
   */
  protected def addDestinationFloor(
    liftState: LiftState,
    destinationFloor: Floor
  ): LiftState = {

    val newState: Either[Floor, Route] = liftState.state match {

      // lift is moving but this destination floor is already scheduled
      case right @ Right(route) if route.destinationFloors.exists(_ == destinationFloor) => {
        logger.debug(s"nothing to schedule for lift $liftState as destination $destinationFloor is already on the destination floors list (${route.destinationFloors})")
        right
      }

      // lift is moving and this destination floor is not scheduled
      case Right(route) => {

        val destinationFloors: Seq[Floor] = sortDestinationsFloors(
          currentFloor = route.currentFloor,
          liftDirection = liftState.direction.get,
          destinationFloors = route.destinationFloors :+ destinationFloor
        )

        logger.debug(s"lift's-in-motion ${liftState.lift} (currently at ${liftState.currentFloor}) schedule ${route.destinationFloors} will be extended by $destinationFloor, new schedule is: $destinationFloors")

        Right(
          route.copy(
            destinationFloors = destinationFloors
          )
        )
      }

      // lift is not moving and is on the same floor
      case Left(currentFloor) if (currentFloor == destinationFloor) => {
        logger.debug(s"nothing to do since lift $liftState is currently waiting on $currentFloor and destination floor is the same ($destinationFloor)")
        Left(currentFloor)
      }

      // lift is not moving but is on different floor
      case Left(currentFloor) => {

        logger.debug(s"waiting ${liftState.lift} at ${liftState.currentFloor} will be send to $destinationFloor")

        Right(
          new Route(
            currentFloor = currentFloor,
            destinationFloors = Seq(destinationFloor)
          )
        )
      }

    }

    liftState.copy(
      state = newState
    )
  }

  /**
   * Sorting destination floors so the lift will have them nicely along current direction (then after a change of
   * direction floor will be in correct order).
   */
  protected def sortDestinationsFloors(currentFloor: Floor, liftDirection: Direction.DirectionType, destinationFloors: Seq[Floor]): Seq[Floor] =
    liftDirection match {

      case Direction.Down => {
        val (onTheLiftsWay, onTheLiftsWayBack): (Seq[Floor], Seq[Floor]) = destinationFloors.partition(_.number < currentFloor.number)
        onTheLiftsWay.sortBy(_.number)(Ordering.Int.reverse) ++ onTheLiftsWayBack.sortBy(_.number)
      }

      case Direction.Up => {
        val (onTheLiftsWay, onTheLiftsWayBack): (Seq[Floor], Seq[Floor]) = destinationFloors.partition(_.number > currentFloor.number)
        onTheLiftsWay.sortBy(_.number) ++ onTheLiftsWayBack.sortBy(_.number)(Ordering.Int.reverse)
      }
    }

  /**
   * This method is responsible for choosing the right lift.
   *
   * @param shaftsState state of all shafts
   * @param passengersFloor floor from which request comes
   * @param direction where passenger would like to go
   */
  def chooseLiftToHandlePickupRequest(
    shaftsState: ShaftsState,
    passengersFloor: Floor,
    direction: Direction.DirectionType
  ): Lift = {

    var lift: Option[Lift] = None

    // let's choose closest moving lift that goes in the same direction (that passenger wants) and can take more passengers
    lift = shaftsState.
      filter(_.shaft.containsFloor(passengersFloor)).
      filter(_.liftState.direction.isDefined).
      filter(_.liftState.direction.get == direction).
      filter(_.isFloorOnTheWay(passengersFloor).getOrElse(false)).
      filter(_.liftState.lift.canTakeMorePassengers).
      map(ss => ss -> ss.liftState.distance(passengersFloor)).
      toSeq.
      sortBy(_._2).
      map(_._1).
      map(_.liftState.lift).
      headOption

    if (lift.isEmpty) {
      // let's choose closest waiting lift that can take more passengers (well, waiting lift should be also an empty lift)
      lift = shaftsState.
        filter(_.shaft.containsFloor(passengersFloor)).
        filter(_.liftState.isWaiting).
        filter(_.liftState.lift.canTakeMorePassengers).
        map(ss => ss -> ss.liftState.distance(passengersFloor)).
        toSeq.
        sortBy(_._2).
        map(_._1).
        map(_.liftState.lift).
        headOption
    }

    // if lift is still not chosen then let's take any lift that can take one more passenger
    lift match {

      case Some(lift) => lift

      case None => shaftsState.
        filter(_.shaft.containsFloor(passengersFloor)).
        filter(_.liftState.lift.canTakeMorePassengers).
        map(_.liftState.lift).
        headOption.
        getOrElse(throw new Exception(s"Could not find any lift that will handle pickup request for passenger at floor $passengersFloor that would like to go $direction."))
    }
  }

  /**
   * Analyzes destination floors.
   *
   * Each lift that is moving has its own schedule, the destination floors list ([[org.lift.problem.model.lift.Route]]).
   * Lift is moving to the closest one. Once reached it will be removed and decision will be taken: should lift move on
   * to next destination floor (if there is any) or there is no more destination floors (so lift just stays where it is).
   */
  def analyzeDestinationFloors(shaftsState: ShaftsState): ShaftsState = {

    logger.debug(s"analyze destination floors in all ${shaftsState.size} shafts")

    shaftsState.copy(
      shafts = shaftsState.shafts.map(analyzeDestinationFloors(_))
    )
  }

  protected def analyzeDestinationFloors(shaftState: ShaftState): ShaftState = {

    val newState: Either[Floor, Route] = shaftState.liftState.state match {

      // lift is not moving
      case left @ Left(_) => {
        logger.debug(s"lift ${shaftState.liftState} is not moving so nothing to do")
        left
      }

      // lift is moving
      case Right(route) => if (route.currentFloor == route.destinationFloors.head) {

        logger.debug(s"lift ${shaftState.liftState.state} reached destination ${route.destinationFloors.head} on its schedule ${route.destinationFloors}, after it will go to ${route.destinationFloors.tail}")

        if (route.destinationFloors.size == 1) {
          logger.debug(s"${shaftState.liftState} has nothing to do, stopping")
          Left(route.currentFloor)
        } else {
          logger.debug(s"${shaftState.liftState} will now go to ${route.destinationFloors.tail.head}")
          Right(
            route.copy(
              destinationFloors = route.destinationFloors.tail
            )
          )
        }

      } else {
        Right(route)
      }
    }

    shaftState.copy(
      liftState = shaftState.liftState.copy(
        state = newState
      )
    )
  }
}
