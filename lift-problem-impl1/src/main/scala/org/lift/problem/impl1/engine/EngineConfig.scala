package org.lift.problem.impl1.engine

/**
 * Configuration for the engine.
 *
 * @param maxCapacity maximal capacity (number of passengers in the lift that this engine can handel)
 */
case class EngineConfig(
  maxCapacity: Int
) {

  require(maxCapacity > 0, "maxCapacity should be greater than zero")

}