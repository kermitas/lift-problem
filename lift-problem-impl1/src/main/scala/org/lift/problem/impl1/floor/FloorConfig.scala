package org.lift.problem.impl1.floor

/**
 * Configuration for the floor.
 */
case class FloorConfig(
  number: Int,
  name: String
) {

  require(name != null && name.nonEmpty, "name can not be null or empty")

}
