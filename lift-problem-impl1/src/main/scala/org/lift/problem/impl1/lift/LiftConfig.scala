package org.lift.problem.impl1.lift

/**
 * Configuration for the lift.
 */
case class LiftConfig(name: String) {

  require(name != null && name.nonEmpty, "name can not be null or empty")

}