package org.lift.problem.impl1.shaft

import org.lift.problem.impl1.engine.EngineConfig
import org.lift.problem.impl1.lift.LiftConfig

/**
 * Configuration for shaft in some building.
 *
 * Working shaft's setup consist of:
 * - [[org.lift.problem.impl1.lift.LiftConfig]],
 * - [[org.lift.problem.impl1.shaft.ShaftConfig]],
 * - [[org.lift.problem.impl1.engine.EngineConfig]].
 */
case class ShaftSetupConfig(
  shaft: ShaftConfig,
  lift: LiftConfig,
  engine: EngineConfig
) {

  require(shaft != null, "shaft can not be null")
  require(lift != null, "lift can not be null")
  require(engine != null, "engine can not be null")

}
