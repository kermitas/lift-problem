package org.lift.problem.impl1.shaft

import org.lift.problem.impl1.floor.FloorConfig
import org.lift.problem.model.floor.Floor

/**
 * Configuration for the shaft.
 *
 * @param floors floors in the lift shaft,
 *               order matters, first position on the list should represents lowest floor
 * @param liftFloorOnSystemInit defines on which floor lift should be when we initialize whole system,
 *                              for example: for floors [ (-1,parking), (0,main hall), (1,first floor) ] we can define that
 *                              when system start lift will be at 'main hall' (0)
 */
case class ShaftConfig(
  name: String,
  floors: Seq[FloorConfig],
  liftFloorOnSystemInit: Int
) extends Iterable[FloorConfig] {

  require(name != null && name.nonEmpty, "name can not be null or empty")
  require(floors != null, "floors can not be null")
  require(floors.size > 1, "floors should contain more than one floor")
  require(floors.map(_.number).toSet.size == floors.size, "floors should have unique numbers")
  require(floors.map(_.name).toSet.size == floors.size, "floors should have unique names")
  floors.tail.foldLeft(floors.head) { (prependingFloor, followingFloor) =>
    require(prependingFloor.number < followingFloor.number, s"following floor $followingFloor should have greater number than prepending one ($prependingFloor)")
    followingFloor
  }
  require(containsFloor(liftFloorOnSystemInit), s"liftFloorOnSystemInit ($liftFloorOnSystemInit) should be one of ${floors.map(_.number)}")

  override def iterator: Iterator[FloorConfig] = floors.iterator

  /**
   * Checks if passed floor number belongs to this shaft.
   */
  protected def containsFloor(floorNumber: Int): Boolean = floors.exists(_.number == floorNumber)
}
