package org.lift.problem.impl1

import com.typesafe.config.{ConfigFactory, Config}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import org.lift.problem.impl1.shaft.ShaftSetupConfig

object LiftControlSystemImpl1Config {

  def read(
    configPath: String,
    config: Config = ConfigFactory.load()
  ): LiftControlSystemImpl1Config =
    ConfigFactory.load.as[LiftControlSystemImpl1Config](configPath)
}

/**
 * Configuration of [[org.lift.problem.impl1.LiftControlSystemImpl1]].
 *
 * @param shaftsSetup members of one logical group of lifts (set of lifts),
 *                    for example: in the building all lifts in main hall we can call a 'set of lifts' (while still in the
 *                    same building there can be other sets of lifts)
 */
case class LiftControlSystemImpl1Config(
  shaftsSetup: Seq[ShaftSetupConfig]
) extends Iterable[ShaftSetupConfig] {

  require(shaftsSetup != null && shaftsSetup.nonEmpty, "shaftsSetup can not be null or empty")

  override def iterator: Iterator[ShaftSetupConfig] = shaftsSetup.iterator

}
