package org.lift.problem.impl1

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.lift.problem.LiftControlSystem
import org.lift.problem.model.floor.Floor
import org.lift.problem.model.lift.{Direction, Lift}
import org.lift.problem.model.shaft.{ShaftState, ShaftsState}
import scala.collection.mutable.ListBuffer

object LiftControlSystemImpl1 extends LazyLogging {

  /**
   * Factory method.
   */
  def apply(config: LiftControlSystemImpl1Config): LiftControlSystem = {
    logger.debug(s"initializing ${classOf[LiftControlSystemImpl1].getSimpleName} with configuration $config")
    new LiftControlSystemImpl1(config)
  }

}

/**
 * Implementation of LiftControlSystem.
 *
 * This implementation is not 'thread safe' and should be called just by one thread at a time (for example: instance of
 * this class can be kept inside of Akka actor).
 *
 * @note 2015-11-23 Artur Stanek: "impl1" in package name and "-Impl1" in the name of class is not the best naming
 *       practise, it was used here just as a name of reference implementation,
 *       usually it should be less abstract like "org.lift.problem.otis", "LiftProblemOtis" if famous lift maker
 *       [[http://www.otis.com/ Otis]] would like to deliver its own implementation
 */
protected[impl1] class LiftControlSystemImpl1(config: LiftControlSystemImpl1Config)
  extends LiftControlSystem with LazyLogging {

  protected val util = new LiftControlSystemImpl1Util

  protected var state: ShaftsState = {
    val initialState = util.createInitialState(config)
    logger.debug(s"starting with state $initialState")
    initialState
  }

  /**
   * (lift that was assigned -> floor on which (future) passenger waits -> floor that passenger will choose to go once entered the lift)
   */
  protected val waitingPassengers = new ListBuffer[(Lift, Floor, () => Floor)]

  override def shaftsState: ShaftsState = state

  /**
   * Not supported by this implementation.
   *
   * @note 2015-11-23 Artur Stanek: this implementation starts lifts state machine with lifts located at configured
   *       floor ([[org.lift.problem.impl1.shaft.ShaftConfig.liftFloorOnSystemInit]]) and does not allow any external
   *       service to modify living state.
   *       State once initialized can not be modify.
   */
  override def update(
    lift: Lift,
    currentFloor: Floor,
    destinationFloor: Floor
  ): Unit =
    throw new UnsupportedOperationException(s"This implementation does not support setting the state of particular ${classOf[Lift].getSimpleName}.")

  override def simulationStep: Unit = {

    // do one step of simulation
    val currentState = state
    val nextState = util.simulationStep(state)
    logger.debug(s"doing one simulation step:\ncurrent state = $currentState,\nnext state    = $nextState")
    state = nextState

    state = util.analyzeDestinationFloors(state)

    processEnteredPassengers
  }

  override def pickupRequest(
    passengersFloor: Floor,
    destinationDirection: Direction.DirectionType,
    floorThatPassengerWillChooseAfterEnteringTheLift: () => Floor
  ): Unit = {

    logger.debug(s"got pickup request from floor $passengersFloor with direction $destinationDirection")

    require(
      state.exists { shaftState: ShaftState =>
        shaftState.shaft.containsFloor(passengersFloor)
      },
      s"currentFloorNumber (${passengersFloor.number}) does not belong to any of available shafts"
    )

    destinationDirection match {
      case Direction.Up => require(
        state.map(_.shaft.highestFloor.number).filter(_ > passengersFloor.number).nonEmpty,
        s"passenger can not go up while being on the highest floor $passengersFloor"
      )
      case Direction.Down => require(
        state.map(_.shaft.lowestFloor.number).filter(_ < passengersFloor.number).nonEmpty,
        s"passenger can not go down while being on the lowest floor $passengersFloor"
      )
    }

    val liftChosenToHandlePickupRequest: Lift = util.chooseLiftToHandlePickupRequest(shaftsState, passengersFloor, destinationDirection)

    logger.debug(s"$liftChosenToHandlePickupRequest was chosen to go for waiting passenger at floor $passengersFloor (that wants to go $destinationDirection)")

    waitingPassengers += ((liftChosenToHandlePickupRequest, passengersFloor, floorThatPassengerWillChooseAfterEnteringTheLift))

    state = util.addDestinationFloor(state, liftChosenToHandlePickupRequest, passengersFloor)
  }

  /**
   * Process passengers that entered the lift.
   */
  protected def processEnteredPassengers: Unit = {
    val currentState = state

    currentState.foreach { shaftsState: ShaftState =>
      waitingPassengers --= waitingPassengers.filter {
        case (lift, passengersFloor, floorThatPassengerWillChooseAfterEnteringTheLift) => {

          if (shaftsState.liftState.lift == lift && shaftsState.liftState.currentFloor == passengersFloor) {
            val destinationFloor = floorThatPassengerWillChooseAfterEnteringTheLift()
            logger.debug(s"passenger entered $lift and chose $destinationFloor")
            state = util.addDestinationFloor(state, lift, destinationFloor)
            true
          } else {
            false
          }

        }
      }
    }
  }
}
