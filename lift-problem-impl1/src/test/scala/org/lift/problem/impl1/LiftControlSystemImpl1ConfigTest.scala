package org.lift.problem.impl1

import org.scalatest.{FeatureSpec, ShouldMatchers}

object LiftControlSystemImpl1ConfigTest {
  val configKeyName = "testLiftSet"
}

class LiftControlSystemImpl1ConfigTest extends FeatureSpec with ShouldMatchers {

  import LiftControlSystemImpl1ConfigTest._

  scenario(s"automatically reading ${classOf[LiftControlSystemImpl1Config].getSimpleName}") {

    val config: LiftControlSystemImpl1Config = LiftControlSystemImpl1Config.read(configKeyName)

    info(s"config = $config")
  }

  scenario(s"equality test of two ${classOf[LiftControlSystemImpl1Config].getSimpleName}") {

    val config1: LiftControlSystemImpl1Config = LiftControlSystemImpl1Config.read(configKeyName)
    val config2: LiftControlSystemImpl1Config = LiftControlSystemImpl1Config.read(configKeyName)

    config1 shouldEqual config2
    config2 shouldEqual config1
    config1.hashCode shouldBe config2.hashCode

  }
}
