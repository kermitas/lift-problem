#lift-problem-impl1
This project contains demo implementation of The Lift Problem (`lift-problem-api`).

As `lift-problem-api` is a set of interfaces and models it can have more implementations in the future.

"-impl1" in project name is not the best naming practise, it was used here just as a name of reference implementation. 
Usually it should be less abstract like "lift-problem-otis" if famous lift maker [Otis](http://www.otis.com/) would like
to deliver its own implementation.