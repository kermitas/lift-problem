package org.lift.problem

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.lift.problem.impl1.{LiftControlSystemImpl1Config, LiftControlSystemImpl1}
import org.lift.problem.model.floor.Floor
import org.lift.problem.model.lift.Direction

object Main extends LazyLogging {

  protected val configKeyName = "liftSet1" // "liftSet1" or "liftSet2" (as configured in application.conf)

  def main(args: Array[String]): Unit = {
    println("Hello from lift-problem/lift control system!")

    logger.debug("Hello from lift-problem/lift control system!")

    val liftControlSystem: LiftControlSystem = LiftControlSystemImpl1(
      config = LiftControlSystemImpl1Config.read(configKeyName)
    )

    logger.debug(s"state = ${liftControlSystem.shaftsState}")

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    logger.debug("----------- placing pick up request -----------")

    liftControlSystem.pickupRequest(
      passengersFloor = new Floor(-1, "parking"),
      destinationDirection = Direction.Up,
      floorThatPassengerWillChooseAfterEnteringTheLift = () => new Floor(3, "Floor 3")
    )

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep

    liftControlSystem.simulationStep
  }

}