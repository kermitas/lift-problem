#lift-problem-main
This is a runnable project.

## Usage
Please see **Grabbing all JARs, generating distribution, run** of `README.md` located in root folder.

## Logging (Logback)
Logging is done via *Logback*, you can provide your own configuration file `logback.xml` (for example via `java -Dlogback.configurationFile=/path/to/config.xml com.company.MyAppMain`). Please see default file `src/pack/config/default/logback.xml` (or `config/logback.xml` if you use generated distribution).