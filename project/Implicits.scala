import  sbt._

import language.implicitConversions

/**
 * This class adds implicit conversions for sequences of projects which are in default case handled by SBT.
 */
object Implicits {
  /**
   * Converts sequence of projects to sequence of project references
   *
   * Assuming we are creating project '''P''' we can make it like this
   *
   * {{{
   *   object P {
   *     def apply(name: String, base : String, projectDependencies: Seq[Project]) = Project(
   *       id = name,
   *       base = file(base),
   *       aggregate = projectDependencies,
   *       delegates = projectDependencies
   *     )
   *   }
   * }}}
   *
   * Normally done in sbt.ProjectExtra.configDependencyConstructor
   *
   * @param projects sequence of projects
   * @return Sequence of project references
   */
  implicit def projectsToProjectRefs(projects: Seq[Project]): Seq[ProjectReference] = projects.map(Project.projectToRef)

  /**
   * Converts sequence of projects to sequence of classpath dependencies
   *
   * Assuming we are creating project '''P''' we can make it like this
   *
   * {{{
   * object P {
   *   def apply(name: String, base : String, projectDependencies: Seq[Project]) = Project(
   *     id = name,
   *     base = file(base),
   *     dependencies = projectDependencies
   *   )
   * }
   * }}}
   *
   * Normally done in sbt.ProjectExtra.classpathDependency
   *
   * @param projects sequence of projects
   * @return Sequence of classpath dependencies
   */
  implicit def projectsToClasspathDependencies(projects: Seq[Project]): Seq[ClasspathDep[ProjectReference]] = projectsToProjectRefs(projects).map(p => Project.classpathDependency(p))
}
