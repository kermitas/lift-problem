object Build extends sbt.Build {

  implicit val artifactConfig = ArtifactConfig(
    name = "lift-problem",
    organization = "org.stanek",
    version = "0.1.0-SNAPSHOT"
  )

  // ==== projects definition

    // ---- models

  val model = ProjectLiftProblemModel()

  // ---- APIs

  val api = ProjectLiftProblemApi(model)

  // ---- implementations

  val impl1 = ProjectLiftProblemImpl1(api)

  // ---- executable projects

  lazy val main = ProjectLiftProblemMain(impl1)
  
}
