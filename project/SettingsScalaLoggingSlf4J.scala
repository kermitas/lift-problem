import sbt._
import Keys._

object SettingsScalaLoggingSlf4J {
  def apply(scope: Configuration = Compile) = Seq(
    libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2" % scope
  )
}
