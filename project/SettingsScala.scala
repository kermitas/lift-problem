import sbt._
import Keys._

object SettingsScala {

  sealed class ScalaVersion(val version: String)
  object v210 extends ScalaVersion("2.10.6")
  object v211 extends ScalaVersion("2.11.7")

  def apply(sv: Seq[ScalaVersion]) = scalaByType(sv) ++ 
    configureScalac
 
  protected def configureScalac = Seq(
    scalacOptions                   ++= Seq("-feature", "-deprecation", "-unchecked", "-Xfatal-warnings"),
    scalacOptions in (Compile, doc) ++= Seq("-implicits")

    // to generate small diagrams in ScalaDoc please enable this line manually
    //scalacOptions in (Compile, doc) ++= Seq("-diagrams") // generates diagrams in documentation, I am not sure if you should have
                                                           // installed Graphviz http://www.graphviz.org/ (the "dot" command in your shell)
  )

  /**
   * First version will be used as current version.
   *
   * First and all others will be used as definition of cross-compilation.
   */
  protected def scalaByType(sv: Seq[ScalaVersion]) = Seq(
    scalaVersion := sv.head.version,
    crossScalaVersions := sv.map(_.version)
  )

}
