import sbt.Keys._

/**
 * Artifact settings.
 */
object SettingsArtifact {
  def apply(artifactConfig: ArtifactConfig) = Seq(
    name         := artifactConfig.name,
    version      := artifactConfig.version,
    organization := artifactConfig.organization
  )
}


