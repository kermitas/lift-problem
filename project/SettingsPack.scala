import xerial.sbt.Pack.packAutoSettings

/**
 * sbt-pack settings.
 */
object SettingsPack {
  def apply() = packAutoSettings
}
