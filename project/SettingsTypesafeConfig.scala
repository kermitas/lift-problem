import sbt._
import Keys._

object SettingsTypesafeConfig {

  def apply(scope: Configuration = Compile) = Seq(
    libraryDependencies += "com.typesafe" % "config" % "1.3.0" % scope
  )

}