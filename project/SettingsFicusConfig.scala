import sbt._
import Keys._

object SettingsFicusConfig {
  def apply(scope: Configuration = Compile) = Seq(

    libraryDependencies += {

      val version = CrossVersion partialVersion scalaVersion.value match {
        case Some((2, scalaMajor)) if scalaMajor >= 11 => "1.1.2"
        case Some((2, 10)) => "1.0.1"
      }

      "net.ceedubs" %% "ficus" % version % scope
    }
  )
}
