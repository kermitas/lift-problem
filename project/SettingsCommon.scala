import sbt._
import Keys._

/**
 * This file contains common settings.
 * Those settings needs to be implemented by all projects.
 */
object SettingsCommon {

  def apply(artifactConfig: ArtifactConfig, scalaVersions : SettingsScala.ScalaVersion*) =
    SettingsArtifact(artifactConfig) ++
    SettingsScala(scalaVersions) ++
    SettingsScalaTest() ++
    SettingsDependencyGraph() ++
    SettingsScalariform() ++
    SettingsPack()
}

