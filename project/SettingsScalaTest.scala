import sbt._
import Keys._

object SettingsScalaTest {
  def apply() = dependencies ++ configureParallelExecutionOfTests ++ configureScalaTest

  protected def dependencies = Seq(
    libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % Test
  )

  protected def configureScalaTest = Seq(

    // make SBT showing full stack trace of exceptions during tests (http://www.scalatest.org/user_guide/using_scalatest_with_sbt)
    testOptions in Test += Tests.Argument("-oDF")

  )

  protected def configureParallelExecutionOfTests = Seq(
    parallelExecution in Test := false,
    parallelExecution in ThisBuild := false
  )
}
