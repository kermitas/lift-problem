// This file contains a list of SBT plugins.

// === IntelliJ Idea project generator (`gen-idea` command)

resolvers += Resolver.sonatypeRepo("snapshots")
addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.7.0-SNAPSHOT")

// === Scalariform (https://github.com/sbt/sbt-scalariform)

resolvers += Resolver.sonatypeRepo("releases")
addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.5.1")

// === dependency graph (`dependency-graph-ml` command)

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.5")

// === pack (`pack` command)

resolvers += "Maven Central Repository" at "http://repo1.maven.org/maven2/"
addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.7.7")

// In case of SBT problems:
//   I am not sure but this line helps to avoid macro errors thrown by SBT while opening project.
//   In this line I include scala-reflect again because sbt-pack uses older Scala (that is how I understand this).
//
//  libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value

// === simple statistics (`stats` command)

addSbtPlugin("com.orrsella" % "sbt-stats" % "1.0.5")

// ===

