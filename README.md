#lift-problem
This is set of projects that deal with The Lift Problem.

More in **README.md** of `lift-problem-main`.

## Usage
- use it (or one of sub-projects) as your dependency:

    ```scala
    libraryDependencies += "org.stanek" %% "lift-problem" % "xx.yy.zz-SNAPSHOT"  // instead of 'lift-problem' you can use directly one of sub-projects (like `lift-problem-api`); instead of 'xx.yy.zz-SNAPSHOT' please use latest version from project/Build.scala
    ```

## Configuration
Please find *reference.conf* and/or *application.conf* files located in sub-projects. Please see [Typesafe Config](https://github.com/typesafehub/config) for more information about configuration files.

## SBT
#### Project structure
This is SBT multi-project. Command executed in one project will be forwarded down to its all sub-projects.

To refresh SBT configuration (after modifying configuration files in `project` subfolder) use `reload` command or restart SBT).

#### Compile
- clone into your local drive: `git clone https://bitbucket.org/kermitas/lift-problem`
- go to root folder (`cd lift-problem`)
- start SBT `sbt`
- to compile:
    - stay in root project or goto whatever project you like, for example `project lift-problem-api`,
    - execute `compile` command
    - to work in continuous compilation use `~compile`
    - **the most preferred way is to always compile with tests** `~test:compile` 

#### Run
- you can run any executable sub-project (please find them in `project/Build.scala`) by executing `run` command (first remember to switch to that project, for example `project lift-problem-main`)

#### Cross compilation
- many commands (like `compile`) prefixed with '+' will perform cross compilation

#### Documentation (ScalaDoc)
- use `doc` command to generate documentation
- **temporary not working** as more time should be put to configure SBT to recognize some classes from Scala standard library (like *scala.Some*, *scala.None*, *scala.Left* and so on)

#### Unit tests
- `test` command will execute unit tests

#### IntelliJ Idea project
- if you are using [IntelliJ Idea](http://www.jetbrains.com/idea/) you can generate Idea project using `gen-idea` command

#### Grabbing all JARs, generating distribution, run
- you can use `pack` command to generate distribution with all JAR files
- generated distribution will be in `target/pack`
    - distributions will be generated for all sub-projects but only those with main class will be runnable (please find them in `project/Build.scala`)
- if you want to run it:
    - first be sure that there is distribution folder `target/pack`, if there is no that means that this project can not be run (for example: it does not have main class defined), if yes change to this folder `cd target/pack`
    - run it be executing one of scripts in `bin` folder (for example: by executing `bin/run`)

#### Statistics
- `stats` command will print statistics (like number of Scala files, Java files, number of lines etc.)

#### Dependency graph
- project is using *sbt-dependency-graph* SBT plugin, execute `dependency-graph-ml` command to generate .graphml files in all *target* directories of all sub-projects (you can use [yEd](http://www.yworks.com/en/products_yfiles_about.html) to open them, then choose *Layout > Hierarchical*)

#### Code formatting
- code format is guarded by [Scalariform](https://github.com/daniel-trinh/scalariform), formatting is done before each compilation
- formatting rules are consistent for all projects of this SBT multi-project and are defined in `project/SettingsScalariform.scala`
- **unformatted code is not allowed in this project**

## GIT (source repository)
This repository works in branching model described in "[A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)".
