package org.lift.problem.model.shaft

import org.lift.problem.model.Model
import org.lift.problem.model.floor.Floor

/**
 * Represents lift shaft.
 *
 * @param floors floors in the lift shaft,
 *               order matters, first position on the list should represents lowest floor
 */
case class Shaft(
  name: String,
  floors: Seq[Floor]
) extends Iterable[Floor] with Model {

  require(name != null && name.nonEmpty, "name can not be null or empty")
  require(floors != null, "floors can not be null")
  require(floors.size > 1, "floors should contain more than one floor")
  require(floors.map(_.number).toSet.size == floors.size, "floors should have unique numbers")
  require(floors.map(_.name).toSet.size == floors.size, "floors should have unique names")
  floors.tail.foldLeft(floors.head) { (prependingFloor, followingFloor) =>
    require(prependingFloor.number < followingFloor.number, s"following floor $followingFloor should have greater number than prepending one ($prependingFloor)")
    followingFloor
  }

  override def iterator: Iterator[Floor] = floors.iterator

  /**
   * Checks if passed floors belongs to this shaft.
   */
  def containsFloors(floors: Seq[Floor]): Boolean = floors.forall(containsFloor)

  /**
   * Checks if passed floor belongs to this shaft.
   */
  def containsFloor(floor: Floor): Boolean = floors.exists(_ == floor)

  /**
   * @return [[scala.Some]] with following (higher) floor, [[scala.None]] if there is no following floor
   */
  def followingFloor(currentFloor: Floor): Option[Floor] = {
    require(containsFloor(currentFloor), s"currentFloor ($currentFloor) should be one of $floors")
    floors.filter(_.number > currentFloor.number).headOption
  }

  /**
   * @return [[scala.Some]] with prepending (lower) floor, [[scala.None]] if there is no prepending floor
   */
  def prependingFloor(currentFloor: Floor): Option[Floor] = {
    require(containsFloor(currentFloor), s"currentFloor ($currentFloor) should be one of $floors")
    floors.filter(_.number < currentFloor.number).lastOption
  }

  /**
   * @return the lowest floor in this shaft
   */
  def lowestFloor: Floor = floors.head

  /**
   * @return the highest floor in this shaft
   */
  def highestFloor: Floor = floors.last
}
