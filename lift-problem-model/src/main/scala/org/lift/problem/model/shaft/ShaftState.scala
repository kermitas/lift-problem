package org.lift.problem.model.shaft

import org.lift.problem.model.Model
import org.lift.problem.model.lift.{Direction, LiftState, Route}
import org.lift.problem.model.floor.Floor

/**
 * Represents the state of the shaft.
 */
case class ShaftState(
  shaft: Shaft,
  liftState: LiftState
) extends Model {

  require(shaft != null, "shaft can not be null")
  require(liftState != null, "liftState can not be null")

  liftState.state.left.map { floor: Floor =>
    require(shaft.containsFloor(floor), s"current floor of waiting lift ($floor) needs to be one of ${shaft.floors}")
  }

  liftState.state.right.map { route: Route =>
    require(shaft.containsFloor(route.currentFloor), s"current floor of lift in motion (${route.currentFloor}) needs to be one of ${shaft.floors}")
    require(shaft.containsFloors(route.destinationFloors), s"destination floors of lift in motion (${route.destinationFloors}) needs to be one of ${shaft.floors}")
  }

  /**
   * Tells if passed [[org.lift.problem.model.floor.Floor]] is on the way of the lift.
   *
   * For example: let's imagine that lift is in motion close to floor number 3 and is going up; by asking this method about
   * floor number 4 (or higher) we will get true, but while asking about floor number 3 (or less) we will get false.
   *
   * @return None if lift is not moving
   */
  def isFloorOnTheWay(floor: Floor): Option[Boolean] = {

    require(shaft.exists(_ == floor), s"floor ($floor) needs to be one of ${shaft.floors}")

    liftState.state match {

      case Left(_) => None

      case Right(route) => liftState.direction.map { direction: Direction.DirectionType =>
        direction match {
          case Direction.Up   => route.currentFloor.number < floor.number
          case Direction.Down => route.currentFloor.number > floor.number
        }
      }

    }
  }
}