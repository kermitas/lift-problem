package org.lift.problem.model.lift

import org.lift.problem.model.Model
import org.lift.problem.model.floor.Floor

/**
 * Represents route of lift-in-motion.
 */
case class Route(
  currentFloor: Floor,
  destinationFloors: Seq[Floor]
) extends Model {

  require(currentFloor != null, "currentFloor can not be null")
  require(destinationFloors != null && destinationFloors.nonEmpty, "destinationFloors can not be null")

}