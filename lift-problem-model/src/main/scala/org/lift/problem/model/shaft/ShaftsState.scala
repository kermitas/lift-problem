package org.lift.problem.model.shaft

import org.lift.problem.model.Model

/**
 * @param shafts states of all shafts
 */
case class ShaftsState(
  shafts: Seq[ShaftState]
) extends Iterable[ShaftState] with Model {

  require(shafts != null && shafts.nonEmpty, "shafts can not be null or empty")
  require(shafts.map(_.shaft.name).toSet.size == shafts.size, "shafts should contain unique shafts")

  override def iterator: Iterator[ShaftState] = shafts.iterator
}
