package org.lift.problem.model.floor

import org.lift.problem.model.Model

case class Floor(
  number: Int,
  name: String
) extends Model {

  require(name != null && name.nonEmpty, "name can not be null or empty")

}
