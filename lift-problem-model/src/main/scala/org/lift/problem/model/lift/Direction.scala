package org.lift.problem.model.lift

/**
 * Holds enum that represents direction of the lift.
 */
object Direction {

  require(all.map(_.up).size == all.size, s"${classOf[DirectionType]}.up should be unique")
  require(all.map(_.down).size == all.size, s"${classOf[DirectionType]}.down should be unique")

  def all: Set[DirectionType] = Set(Up, Down)

  /**
   * Represents all types of directions.
   *
   * @note 2015-11-23 Artur Stanek: to make things simpler only vertical lifts are supported
   */
  sealed trait DirectionType {
    def up: Boolean
    def down: Boolean = !up
  }

  case object Up extends DirectionType {
    override final val up: Boolean = true
  }

  case object Down extends DirectionType {
    override final val up: Boolean = false
  }
}