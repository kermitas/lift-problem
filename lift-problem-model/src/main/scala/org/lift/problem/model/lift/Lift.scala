package org.lift.problem.model.lift

import org.lift.problem.model.Model

/**
 * @param maxCapacity maximal capacity (number of passengers in the lift that this engine can handel)
 * @param currentLoad current number of passengers in the lift
 */
case class Lift(
  name: String,
  maxCapacity: Int,
  currentLoad: Int
) extends Model {

  require(name != null && name.nonEmpty, "name can not be null or empty")
  require(maxCapacity > 0, "maxCapacity should be greater than zero")
  require(currentLoad >= 0, "currentLoad should be greater or equal zero")
  require(currentLoad <= maxCapacity, s"currentLoad ($currentLoad) should be smaller or equal maxCapacity ($maxCapacity)")

  def canTakeMorePassengers: Boolean = currentLoad < maxCapacity
}