package org.lift.problem.model.lift

import org.lift.problem.model.Model
import org.lift.problem.model.floor.Floor

/**
 * Represents the state of the lift.
 */
case class LiftState(
  lift: Lift,
  state: Either[Floor, Route]
) extends Model {

  require(lift != null, "lift can not be null")
  require(state != null, "state can not be null")
  require(state.isLeft || state.right != null, "state.right can not be null")
  require(state.isRight || state.left != null, "state.left can not be null")

  /**
   * @return direction of the lift (if this lift is in motion),
   *         [[scala.Some]] with [[org.lift.problem.model.lift.Direction]] when lift is in motion (moving),
   *         [[scala.None]] otherwise
   */
  def direction: Option[Direction.DirectionType] = state match {

    case Left(_) => None

    case Right(route: Route) => if (route.currentFloor.number < route.destinationFloors.head.number)
      Some(Direction.Up)
    else if (route.currentFloor.number > route.destinationFloors.head.number)
      Some(Direction.Down)
    else
      None
  }

  /**
   * @return current floor (no matter if lift is moving or not)
   */
  def currentFloor: Floor = state match {
    case Left(currentFloor) => currentFloor
    case Right(route)       => route.currentFloor
  }

  def inMotion: Boolean = state.isRight

  def isWaiting: Boolean = !inMotion

  /**
   * @return distance between current position of the lift (whether it is moving or not) and passed floor
   */
  def distance(floor: Floor): Int = {
    val currentFloor = state match {
      case Left(currentFloor) => currentFloor
      case Right(route)       => route.currentFloor
    }

    Math.abs(floor.number - currentFloor.number)
  }
}
