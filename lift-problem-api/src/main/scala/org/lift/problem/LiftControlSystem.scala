package org.lift.problem

import org.lift.problem.model.floor.Floor
import org.lift.problem.model.lift.{Lift, Direction}
import org.lift.problem.model.shaft.ShaftsState

/**
 * Base trait that exposes operations over set of lifts.
 */
trait LiftControlSystem {

  /**
   * Query for current state of lift shafts.
   *
   * @return state of shafts in current point of time,
   *         look out: once returned can become immediately invalid (as set of lifts is a dynamic system), use it only
   *         for information purposes
   */
  def shaftsState: ShaftsState

  /**
   * Registers incoming pickup request from passenger that waits on some particular floor.
   *
   * @param passengersFloor the floor on which passenger is currently waiting
   * @param destinationDirection direction of destination (target) floor where passenger would like to go (up or down)
   * @param floorThatPassengerWillChooseAfterEnteringTheLift once passenger would step into the lift she/he will choose
   *                                                         this floor to go to
   */
  def pickupRequest(
    passengersFloor: Floor,
    destinationDirection: Direction.DirectionType,
    floorThatPassengerWillChooseAfterEnteringTheLift: () => Floor
  ): Unit

  /**
   * Updating the state of the lift.
   *
   * @param lift lift that's state should be updated
   * @param currentFloor floor on which lift is currently
   * @param destinationFloor floor where lift should go
   */
  def update(lift: Lift, currentFloor: Floor, destinationFloor: Floor): Unit

  /**
   * Performs one simulation step.
   */
  def simulationStep: Unit

}
